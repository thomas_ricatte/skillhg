'''
Created on 7 mai 2014

@author: thomas
'''
import unittest
import numpy as np

from SkillGraph.model import Player, KnownGame, Team, GameSet
from SkillGraph.calculators import SGCalculator

# import pdb, sys
# import functools
# def debug_on(*exceptions):
#     if not exceptions:
#         exceptions = (AssertionError, )
#     def decorator(f):
#         @functools.wraps(f)
#         def wrapper(*args, **kwargs):
#             try:
#                 return f(*args, **kwargs)
#             except exceptions:
#                 pdb.post_mortem(sys.exc_info()[2])
#         return wrapper
#     return decorator

class SGCalculatorTests(unittest.TestCase):
    def setUp(self):
        self.teams = [Team([Player(2*i),Player(2*i+1)]) for i in range(10)]    
        self.teams[0].add(Player("Obi-Wan Kenobi"))
        print(self.teams)
        
        self.games = [KnownGame(self.teams[i], self.teams[i+1], 3) for i in range(9)]
        print(self.games)
        
        self.GS = GameSet(self.games)
        print(self.GS)
        print("Average number of game played : %s"%self.GS.averageNumberOfParticipations())
        

    def testSimpleDraw(self):
        G = KnownGame(Team(Player(1)),Team(Player(2)),0)
        GS = GameSet([G])
        SG = SGCalculator(GS)
        e = SG.computeSkills()
        print(e)


    def testSimpleSGC(self):
        SGC = SGCalculator(self.GS)
        
        print(SGC._nodeList)
        G = SGC.getSparseGradient().todense()
        Delta = SGC.getSparseLaplacian().todense()
        
        #print("G = %s\n"%G)
        #print("Delta = %s\n"%Delta)
        
        # Gradient check
        self.assertEqual(np.sum(G),0,"Incorrect gradient")
        
        # Symmétrique
        self.assertEqual(np.linalg.norm(np.transpose(Delta)-Delta),0,"Laplacian is not symetric")
        # SMS 0
        self.assertAlmostEqual(np.sum(Delta),0,msg="Laplacian is not SMS0")
        # PSD
        eigs = np.linalg.eigvalsh(Delta)
        self.assertAlmostEqual(np.min(eigs), 0, msg="Laplacian is not psd")
        
    def testSGCwithNoMerge(self):
        SGC = SGCalculator(self.GS,mergeHelperNodes=False)
        
        print(SGC._nodeList)
        Delta = SGC.getSparseLaplacian().todense()
        
        # Symmétrique
        self.assertAlmostEqual(np.linalg.norm(np.transpose(Delta)-Delta),0)
        # SMS 0
        self.assertAlmostEqual(np.sum(Delta),0)
        # PSD
        eigs = np.linalg.eigvalsh(Delta)
        self.assertAlmostEqual(np.min(eigs), 0)
        
    def testSGCwithRawOutcome(self):
        SGC = SGCalculator(self.GS,useRawOutput=True)

        Delta = SGC.getSparseLaplacian().todense()
        
        # Symmétrique
        self.assertAlmostEqual(np.linalg.norm(np.transpose(Delta)-Delta),0)
        # SMS 0
        self.assertAlmostEqual(np.sum(Delta),0)
        # PSD
        eigs = np.linalg.eigvalsh(Delta)
        self.assertAlmostEqual(np.min(eigs), 0)    

    def testSGCwithNoMergeAndRawOutcome(self):
        SGC = SGCalculator(self.GS,useRawOutput=True,mergeHelperNodes=False)

        Delta = SGC.getSparseLaplacian().todense()
        
        # Symmétrique
        self.assertEqual(np.linalg.norm(np.transpose(Delta)-Delta),0)
        # SMS 0
        self.assertAlmostEqual(np.sum(Delta),0)
        # PSD
        eigs = np.linalg.eigvalsh(Delta)
        self.assertAlmostEqual(np.min(eigs), 0)
        
    def testSimpleLearning(self):
        SGC = SGCalculator(self.GS)
        
        ret = SGC.computeSkills()
        
        print("Learnt %s values (for %s players)"%(len(ret), len(self.GS.getPlayers())))
        
        self.assertEqual(len(ret), len(self.GS.getPlayers()), "Number of learnt values is different from the number of players")
        
        for P in ret:
            print("%s => %s"%(P,ret[P]))
            
        predictions = SGC.predictOutcomes(self.GS.getGameList())
        
        for i,game in enumerate(self.GS.getGameList()):
            print("Real: %s ;; Predicted: %s"%(game.getOutcome(),predictions[i]))
            
        learningError = SGC.getLearningError()
        print("Learning error is %s"%learningError)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()