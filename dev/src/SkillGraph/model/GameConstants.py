'''
Created on 6 mai 2014

@author: thomas
'''

import numpy as np

class OutCome(object):
    
    WIN_1 = "WIN_1"
    DRAW = "DRAW"
    WIN_2 = "WIN_2"

    costMatrix = np.matrix([[0,0.5,1],[0.5,0,0.5],[1,0.5,0]])
    
    indices = {
             WIN_1: 0,
             DRAW: 1,
             WIN_2: 2
             }

    @staticmethod
    def error(predictedOutcome,realOutcome):
 
        i = OutCome.indices[predictedOutcome]
        j = OutCome.indices[realOutcome]
        
        return OutCome.costMatrix[i,j]
        