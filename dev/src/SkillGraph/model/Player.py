'''
Created on 25 avr. 2014

@author: Thomas Ricatte
'''

class Player(object):
    '''
    An object to represent a Player
    '''

    hashPrefix = "PLAYER::"

    def __init__(self, name):
        '''
        Constructor
        '''
        self._name = str(name)
        
        
    def __eq__(self, obj):
        return isinstance(obj, Player) and obj._name == self._name
    
    def __hash__(self):
        return hash(Player.hashPrefix+self._name)
    
    def __str__(self):
        return "Player %s"%str(self._name)
    
    def __repr__(self):
        return "Player %s"%str(self._name)