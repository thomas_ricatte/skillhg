'''
Created on 7 mai 2014

@author: thomas
'''

from math import sqrt

import numpy as np
from scipy.sparse import csr_matrix

from SkillGraph.model import GameSet, Helper, Player, Game, KnownGame

from SkillGraph.calculators.graphLearner import getLearner
from SkillGraph.model import VirtualPlayer
from SkillGraph.model import OutCome




class SGCalculator(object):
    '''
    A Skill Graph Calculator implemented using a hypernode graph
    '''


    def __init__(self, gameSet, useRawOutput=False, mergeHelperNodes=True, regularize="auto",normalizedLaplacian=False):
        '''
        Constructor
        '''
        
        if not isinstance(gameSet, GameSet):
            raise TypeError("Expected Gameset as first argument")
        
        # Defining the gameSet for the calculator
        self._gameSet = gameSet
        
        # Setting parameters
        self._useRawOutput = useRawOutput
        self._mergeHelperNodes = mergeHelperNodes
        
        if regularize == "auto":
            self._regularizer = self._computeRegularizer()
        else:
            try:
                self._regularizer = float(regularize)
            except:
                raise TypeError("regularize must be 'auto' or a number. Got %s" % type(regularize))
        
        # Initialize node list
        self._initializeNodeList()
        # Build gradient
        self._buildGradient()
        self._buildLaplacian(normalizedLaplacian)
        
        self._learntSkills = None
    
    
    def _computeRegularizer(self):
        '''
        Compute the regularizer using the average number of games played by a player
        '''
        return 0.8 * self._gameSet.averageNumberOfParticipations()
        
    def _refineOutput(self, outcome):
        if self._useRawOutput:
            return outcome
        else:
            return np.sign(outcome)
    
    def _initializeNodeList(self):
               
        # A) Create the helpers nodes and populate the game to helper map
        self._helpersList = []
        rawOutcomes = []
        self._gameToHelpersMap = {}     
        
        if self._mergeHelperNodes:
            # We add a zero helper, we will need it
            rawOutcomes.append(0)
            self._helpersList.append(Helper(0)) 
        
        for game in self._gameSet.getGameList():
            refinedOutcome = self._refineOutput(game.getRawOutcome())
            
            if game.isDraw():
                Hleft = None
                Z = None
            else:
                if self._mergeHelperNodes:
                    # If we merge the helper nodes, we have to check whether a rawOutcomes already exists or not
                    if refinedOutcome in rawOutcomes:
                        idx = rawOutcomes.index(refinedOutcome)
                        Hleft = self._helpersList[idx]
                    else:
                        Hleft = Helper(refinedOutcome)
                        rawOutcomes.append(refinedOutcome)
                        self._helpersList.append(Hleft)
                    
                    # We retrieve the unique zero helper previously created
                    idxZ = rawOutcomes.index(0)
                    Z = self._helpersList[idxZ]
                        
                else:
                    # If we don't merge the helper nodes, the solution is easier: we just create a new pair of nodes
                    # for each game
                    Hleft = Helper(refinedOutcome)
                    self._helpersList.append(Hleft)
                    Z = Helper(0)
                    self._helpersList.append(Z)
            
            # Finally we update the game to helpers map
            self._gameToHelpersMap[game] = [Hleft, Z]
                    
        # B) Create the full list by concatenating the list of helpers and the list of players        
        self._nodeList = self._helpersList + self._gameSet.getPlayers()        
        
        # C) We add the regularizer node to the list
        self._regularizerNode = VirtualPlayer()
        self._nodeList.append(self._regularizerNode)
        
        
    def _buildGradient(self):
        
        csr_rows = []
        csr_cols = []
        csr_vals = []
        
        def addValue(row, col, val):
            csr_rows.append(row)
            csr_cols.append(col)
            csr_vals.append(val)
        
        # For each game (row)
        for row, game in enumerate(self._gameSet.getGameList()):
            
            # We retrieve the helpers
            [Hleft, Z] = self._gameToHelpersMap[game]
            
            if not game.isDraw():
                # Adding the active helper to the left side
                col = self._nodeList.index(Hleft)
                val = -1
                addValue(row, col, val)
                
                # Computing the new val for Z
                # Base placement for Z is right so the value should be:
                # 1 (compensation for the helper node that we have just added)
                # - differentialOfPlayers (compensation for the additional players in team 2 (right)) 
                # => the value should be the number of additional players in team2
                col = self._nodeList.index(Z)
                val = 1 - game.getDifferentialOfPlayers()
                addValue(row, col, val)
                
            # We add the nodes for the players
            for P in game.getTeam1().getPlayers():
                col = self._nodeList.index(P)
                val = -1
                addValue(row, col, val)
            
            for P in game.getTeam2().getPlayers():
                col = self._nodeList.index(P)
                val = 1
                addValue(row, col, val)
        
        
        # Now add the regularizer edges
        
        
        for node in self._nodeList:
            if isinstance(node, Player):
                # for each player in the node list, we create an edge to R
                row += 1
                col = self._nodeList.index(node)
                val = -sqrt(self._regularizer)
                addValue(row, col, val)
                
                col = self._nodeList.index(self._regularizerNode)
                val = sqrt(self._regularizer)
                addValue(row, col, val)
        
        # One hyperedge per game + one (hyper)edge per player (regularization)
        numRows = len(self._gameSet.getGameList()) + len(self._gameSet.getPlayers())
        # num of cols is always the number of nodes
        numCols = len(self._nodeList)
        
        self._G = csr_matrix((csr_vals, (csr_rows, csr_cols)), shape=(numRows, numCols))
        
    
    def _buildLaplacian(self, normalizedLaplacian):
        # Build the Laplacian as a sparse matrix
        self._Delta = np.transpose(self._G) * self._G
        
        if normalizedLaplacian:
            Darray = self._Delta.diagonal()
            N = self._Delta.shape[0]
            DsqrtMinus = csr_matrix(([val**(-1/2) for val in Darray],(range(N),range(N))),shape=(N,N))
            DeltaNormalized = DsqrtMinus*self._Delta*DsqrtMinus
            self._Delta = DeltaNormalized
    
    def getSparseGradient(self):
        return self._G
    
    def getSparseLaplacian(self):
        return self._Delta
    
    def computeSkills(self):
        
        learner = getLearner("ZGL")
        
        Delta = self.getSparseLaplacian().todense()
        
        Y = np.zeros((len(self._nodeList), 1))
        unknownindices = []
        
        for i, node in enumerate(self._nodeList):
            if isinstance(node, Helper):
                Y[i, 0] = node.getSkill()
            else:
                unknownindices.append(i)
        
        Ylearnt = learner.learn(Delta, Y, unknownindices)
        
        self._learntSkills = {}
        
        i = 0
        for node in self._nodeList:
            if isinstance(node, Player):
                self._learntSkills[node] = Ylearnt[i, 0]
                i += 1
            elif node == self._regularizerNode:
                self._regularizerSkill = Ylearnt[i, 0]
                i += 1
        
        # Updating the mean skill
        self._meanSKill = np.mean(list(self._learntSkills.values()))

        return self._learntSkills
    
    
    def predictRawOutcome(self, G):
        '''
        Predict the outcome from a single game
        '''
        
        if self._learntSkills == None:
            raise RuntimeError("Skills should be computed first")
        
        if not isinstance(G, Game):
            raise TypeError("Expected Game but got %s instead" % type(G))
        
        team1Performance = 0
        for P in G.getTeam1().getPlayers():
            if P in self._learntSkills:
                team1Performance += self._learntSkills[P]
            else:
                team1Performance += self._meanSKill
                
        team2Performance = 0
        for P in G.getTeam2().getPlayers():
            if P in self._learntSkills:
                team2Performance += self._learntSkills[P]
            else:
                team2Performance += self._meanSKill
                
        return team2Performance - team1Performance
    
    
    def predictOutcome(self, G):
        '''
        Predict the outcome from a single game
        '''
        
        rawOutCome = self.predictRawOutcome(G)
                
        if rawOutCome>0:
            return OutCome.WIN_2
        elif rawOutCome<0:
            return OutCome.WIN_1
        else:
            return OutCome.DRAW
        
        
        
    def predictOutcomes(self, gameList):
        '''
        Predict the outcomes from a game list
        '''
        ret = []
        for G in gameList:
            ret.append(self.predictOutcome(G))
            
        return ret
    
    
    def predictRawOutcomes(self, gameList):
        '''
        Predict the outcomes from a game list
        '''
        ret = []
        for G in gameList:
            ret.append(self.predictRawOutcome(G))
            
        return ret
    
    
    def validateOutcomes(self, knownGameList,stats=None):
        '''
        Compute the error from a list of known games
        draw = half loss / half win
        '''
        
        predictedOutcomes = self.predictOutcomes(knownGameList)
        
        error = 0
        predictedDrawCounter = 0
        for i,game in enumerate(knownGameList):
            if not isinstance(game,KnownGame):
                raise TypeError("Need known games for validation")
            
            realOutcome = game.getOutcome()
            predictedOutcome = predictedOutcomes[i]
            
            error += OutCome.error(predictedOutcome, realOutcome)
            
            if predictedOutcome==OutCome.DRAW:
                predictedDrawCounter += 1
            
        if isinstance(stats,dict):
            stats["predictedDrawCounter"] = predictedDrawCounter
           
        return error/float(len(knownGameList))
    
    
    def validateOutcomesRMSE(self, knownGameList):
        '''
        Compute the error from a list of known games
        Result is the root mean square error
        '''
        
        rawPredictedOutcomes = self.predictRawOutcomes(knownGameList)
        
        errors = []
        for i,game in enumerate(knownGameList):
            if not isinstance(game,KnownGame):
                raise TypeError("Need known games for validation")
            
            realOutcome = self._refineOutput(game.getRawOutcome())
            predictedOutcome = rawPredictedOutcomes[i]
            
            square_error = (predictedOutcome - realOutcome)**2
            
            errors.append(square_error)
        
        rmse = np.sqrt(np.mean(errors))
        
        return rmse
    
    
    def getLearningError(self):
        '''
        Returns the learning error if the skills have been computed
        '''
        return self.validateOutcomes(self._gameSet.getGameList())
    
    
    def getLearningErrorRMSE(self):
        '''
        Returns the learning error if the skills have been computed
        '''
        return self.validateOutcomesRMSE(self._gameSet.getGameList())
    
    def getDrawDistribution(self,knownGameList):
        
        draw_values = []
        nondraw_values = []
        for game in knownGameList:
            rawOutcome = self.predictRawOutcome(game)
            if game.getOutcome()==OutCome.DRAW:
                draw_values.append(rawOutcome)
            else:
                nondraw_values.append(rawOutcome)
                
                
        return {"DRAW":draw_values,"NON_DRAW":nondraw_values}
                
