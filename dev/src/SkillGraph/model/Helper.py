'''
Created on 7 mai 2014

@author: thomas
'''

class Helper(object):
    '''
    An object to represent an Helper with fixed skill
    '''
    
    __uid = 0

    hashPrefix = "HELPER::"

    def __init__(self, skill):
        '''
        Constructor
        '''
        self._skill = skill
        self._helperId = Helper.__uid
        Helper.__uid += 1
        
    def getSkill(self):
        return self._skill    
    
        
    def __eq__(self, obj):
        return isinstance(obj, Helper) and obj._helperId == self._helperId
    
    def __hash__(self):
        return hash(Helper.hashPrefix+str(self._helperId))
    
    def __str__(self):
        return "Helper %s (skill=%s)"%(self._helperId,str(self._skill))
    
    def __repr__(self):
        return "Helper %s (skill=%s)"%(self._helperId,str(self._skill))