'''
Created on 20 juin 2012

@author: I066835
'''

import numpy as np;
import scipy.linalg

from SkillGraph.calculators.graphLearner import Learner

class ZGLLearner(Learner):
    
    def __init__(self):
        self.name = "ZGLLearner"
    
    def learn(self,Delta, Y, unknownIndexes):
        Deltauu = Delta[unknownIndexes,:][:,unknownIndexes];
        knownIndexes = [i for i in range(Delta.shape[0]) if i not in unknownIndexes];
        Deltaul = Delta[unknownIndexes,:][:,knownIndexes];
        Yl = np.array(Y[knownIndexes]);
        DeltauuPlus = scipy.linalg.pinvh(Deltauu,rcond=1e-8);
        
        Ytemp = np.dot(Deltaul,Yl);
                
        Yfinal = - np.dot(DeltauuPlus,Ytemp);
        
        return Yfinal;

    
    
