'''
Created on 9 mai 2014

@author: thomas
'''

from SkillGraph.calculators.graphLearner import ZHSLearner, ZGLLearner

instances = {
             "ZGL": ZGLLearner(),
             "ZHS": ZHSLearner()
             }

def getLearner(name):
    return instances[name]