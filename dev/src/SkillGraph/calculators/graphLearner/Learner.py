'''
Created on 9 mai 2014

@author: thomas
'''

class Learner(object):
    '''
    Interface for learners
    ''' 

    def __init__(self):
        self.name = "AbstractLearner"
    
    def learn(self,Delta, Y, unknownIndexes, mu):
        raise NotImplementedError()
    
    def getName(self):
        return self.name

    def __str__(self):
        return self.name
    
    def __repr__(self):
        return self.name
