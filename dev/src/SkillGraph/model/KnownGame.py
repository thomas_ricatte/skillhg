'''
Created on 6 mai 2014

@author: thomas
'''

from SkillGraph.model import Game
from SkillGraph.model import OutCome

class KnownGame(Game):
    '''
    A subclass of Game that include the result of the game
    '''


    def __init__(self, T1,T2, rawOutcome):
        '''
        Constructor
        outcome is the number of points scored by T2 - number of points scored by T1
        If outcome is positive : T2 has won
        If outcome is negative : T1 has won
        If outcome is null : Game is a draw
        '''
        super(KnownGame,self).__init__(T1,T2)
        
        self._rawOutcome = rawOutcome
        
        
    def getOutcome(self):
        '''
        
        '''
        if self._rawOutcome > 0:
            return OutCome.WIN_2
        elif self._rawOutcome < 0:
            return OutCome.WIN_1
        else:
            return OutCome.DRAW
        
    def getRawOutcome(self):
        return self._rawOutcome
        
    def isDraw(self):
        return self.getOutcome()==OutCome.DRAW
    
    def __str__(self):
        return "%s :: %s (%s)"%(super(KnownGame,self).__str__(),self.getOutcome(),self.getRawOutcome())
    
    def __repr__(self):
        return "%s :: %s (%s)"%(super(KnownGame,self).__str__(),self.getOutcome(),self.getRawOutcome())