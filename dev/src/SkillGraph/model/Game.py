'''
Created on 25 avr. 2014

@author: I066835
'''

from SkillGraph.model import Team

class Game(object):
    '''
    classdocs
    '''
    
    __uid = 0

    def __init__(self, T1,T2):
        '''
        Constructor
        '''
        if not isinstance(T1, Team) or not isinstance(T2,Team):
            raise TypeError()
        self._T1 = T1
        self._T2 = T2
        
        if Team.doIntersect(self._T1, self._T2):
            raise ValueError("Teams %s and %s have a non null intersection"%(self._T1,self._T2))
        
        
        self._gameId = Game.__uid
        Game.__uid += 1
        
    def __str__(self):
        return "[G%s] %s VS %s"%(self._gameId,str(self._T1),str(self._T2))
    
    def __repr__(self):
        return "[G%s] %s VS %s"%(self._gameId,str(self._T1),str(self._T2))
    
    def getTeam1(self):
        return self._T1
    
    def getTeam2(self):
        return self._T2
    
    def playerTeam(self,P):
        '''
        Returns 1 if P belongs to the first team, 2 if it belongs to the second team
        and 0 otherwise
        '''
        if self._T1.containsPlayer(P):
            return 1
        elif self._T2.containsPlayer(P):
            return 2
        else:
            return 0
    
    def getDifferentialOfPlayers(self):
        '''
        Return the differential of players between both teams
        i.e., number of players in team 2 - number of players in team 1
        '''
        return len(self._T2.getPlayers())-len(self._T1.getPlayers())
    
    def getPlayers(self):
        '''
        return a list of the involved players
        '''
        ret = self._T1.getPlayers()
        ret += self._T2.getPlayers()
        
        return ret
        
        
    def __hash__(self):
        return hash(self._gameId)
    
    def __eq__(self, other):
        return isinstance(other,Game) and self._gameId==other._gameId