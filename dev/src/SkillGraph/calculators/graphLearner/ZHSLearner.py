'''
Created on 24 mars 2014

@author: I066835
'''


import numpy as np;
import scipy.linalg

from SkillGraph.calculators.graphLearner import Learner

class ZHSLearner(Learner):

    def __init__(self):
        self.name = "ZHSLearner"

    def learn(self,Delta, Y, unknownIndexes, mu):
        
        alpha = 1./(1.+mu)
        DeltaNorm = Delta / np.linalg.norm(Delta)
        
        I = np.eye(np.shape(DeltaNorm)[0])
        theta = I - DeltaNorm
        knownLabels = Y.copy()
        for i in unknownIndexes:
            knownLabels[i,0] = 0
        E = np.matrix(I - alpha*theta)
        Einv = np.matrix(scipy.linalg.pinvh(E,rcond=1e-8))
        Ylearnt = (1-alpha)*Einv*knownLabels
        Yfinal = np.transpose(np.matrix([Ylearnt[i,0] for i in unknownIndexes]))
        
        return Yfinal;

    


            
            
            
            