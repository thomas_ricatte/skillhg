'''
Created on 9 mai 2014

@author: thomas
'''


class VirtualPlayer(object):
    '''
    A virtual player class (used for example to represent a regularizer node)
    '''
    
    def __str__(self):
        return "Virtual Player %s"%(id(self))
        
    def __repr__(self):
        return "Virtual Player %s"%(id(self))