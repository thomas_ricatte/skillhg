from SkillGraph.calculators.graphLearner.Learner import Learner
from SkillGraph.calculators.graphLearner.ZGLLearner import ZGLLearner
from SkillGraph.calculators.graphLearner.ZHSLearner import ZHSLearner

from SkillGraph.calculators.graphLearner.LearnerHelper import getLearner