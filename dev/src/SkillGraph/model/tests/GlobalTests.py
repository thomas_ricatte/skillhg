'''
Created on 25 avr. 2014

@author: I066835
'''
import unittest
from SkillGraph.model import Player, KnownGame, Helper
from SkillGraph.model import Team
from SkillGraph.model import Game
from SkillGraph.model.GameConstants import OutCome
from SkillGraph.model.GameSet import GameSet
# from SkillGraph.model import GameSet

class GlobalTest(unittest.TestCase):


    def setUp(self):
        self.P1 = Player(1)
        self.P1b = Player(1)
        self.P2 = Player(2)
        self.P3 = Player(3)
        self.P4 = Player(4)

    def testPlayer(self):
        self.assertEqual(self.P1, self.P1b, None)
    
        
    def testAddPlayerToTeam(self):
        T = Team()
        T.add(self.P1)
        print(T)
    
    def testAddIterable_to_team(self):
        T = Team()
        T.add([self.P1])
        T.add({self.P1})
        print(T)
        
    def testTeamSize(self):
        T = Team()
        T.add(self.P1)
        T.add([self.P2, self.P3])
        T.add((self.P1, self.P3))
        msg = "Team %s has size %s" % (str(T), T.size())
        self.assertEqual(T.size(), 3, msg)
        print(msg)
        
        self.assertTrue(T.containsPlayer(self.P1))
        self.assertTrue(T.containsPlayer(Player(2)))
        self.assertFalse(T.containsPlayer(self.P4))
        
    def testTeamIntersect(self):
        T1 = Team()
        T1.add(self.P1)
        T2 = Team()
        T2.add([self.P1b, self.P2])
        T3 = Team()
        T3.add(self.P3)
        self.assertTrue(Team.doIntersect(T1, T2), "%s and %s should intersect" % (T1, T2))
        self.assertFalse(Team.doIntersect(T1, T3), "%s and %s should not intersect" % (T1, T3))
        
    def testGame(self):
        T1 = Team()
        T1.add(self.P1)
        T2 = Team()
        T2.add(self.P1b)
        T3 = Team()
        T3.add(self.P3)
        didIntersectError = False
        try:
            G = Game(T1, T2)
            print(G)
        except ValueError:
            didIntersectError = True
            
        self.assertTrue(didIntersectError, "Teams %s and %s should not be allowed to fight against each other" % (T1, T2))
        
        G2 = Game(T1, T3)
        print(G2)
        
        G3 = Game(T1, T3)
        print(G3)
        
        self.assertNotEqual(G2, G3, "%s and %s should be different" % (str(G2), str(G3)))
        self.assertEqual(G2, G2, "%s and %s should be equal" % (str(G2), str(G2)))
        
        self.assertEqual(G2.playerTeam(self.P1), 1)
        self.assertEqual(G2.playerTeam(self.P3), 2)
        self.assertEqual(G2.playerTeam(self.P4), 0)
        
    def testKnownGame(self):
        T1 = Team()
        T1.add(self.P1)
        T3 = Team()
        T3.add(self.P3)
        G = KnownGame(T1, T3, 2)
        self.assertEqual(G.getRawOutcome(), 2, "Outcome should be 2")
        self.assertEqual(G.getOutcome(), OutCome.WIN_2, "Second team has won !")
        
        print(G)
        
        G2 = KnownGame(T1, T3, -6)
        self.assertEqual(G2.getRawOutcome(), -6, "Outcome should be -6")
        self.assertEqual(G2.getOutcome(), OutCome.WIN_1, "First team has won !")
        
        print(G2)
        
    def testGameset(self):
        T1 = Team()
        T1.add(self.P1)
        T3 = Team()
        T3.add(self.P3)
        G1 = KnownGame(T1, T3, 2) 
        G2 = KnownGame(T1, T3, -6)
        G3 = Game(T1, T3)
        
        
        didFailWithNonIterable = False
        try:
            GameSet(G1)
        except TypeError:
            didFailWithNonIterable = True
        
        print("Failing with non iterable: %s" % didFailWithNonIterable)
        self.assertTrue(didFailWithNonIterable, "Should fail with non iterable")
        
        
        GS = GameSet([G1, G2])
        
        print(GS)
        
        didFailWithUnknownGame = False
        try:
            GameSet([G1, G2, G3])
        except TypeError:
            didFailWithUnknownGame = True
        
        print("Failing with unknown game: %s" % didFailWithUnknownGame)
        self.assertTrue(didFailWithUnknownGame, "Unknown game shouldn't be accepted")
        
        anop = GS.averageNumberOfParticipations()
        self.assertEqual(anop, 2.0,) 
        print("ANOP = %s"%anop)


    def test_helper(self):
        H = Helper(12)
        H2 = Helper(12)
        self.assertNotEqual(H, H2)
        self.assertEqual(H, H)
        print(H)
        print(H2)

if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
    
    
