'''
Created on 9 mai 2014
Last updated on 20 novembre 2014

@author: thomas
'''

from distutils.core import setup
setup(
    name = "SkillGraph",
    packages = ["SkillGraph", "SkillGraph.model", "SkillGraph.calculators", "SkillGraph.calculators.graphLearner"],
    version = "0.1.1",
    description = "SkillGraph: The Hypernode Skill Rating System",
    author = "Thomas Ricatte",
    author_email = "thomas.ricatte@inria.fr",
    url = "",
    download_url = "",
    keywords = [],
    classifiers = [],
    long_description = """\
SkillGraph: A library to learn skills using hypernode graphs
-------------------------------------------------------------

Compatible Python2/Python3

"""
)
