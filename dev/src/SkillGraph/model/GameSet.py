'''
Created on 5 mai 2014

@author: thomas
'''

import numpy as np

from SkillGraph.model.KnownGame import KnownGame

class GameSet(object):
    '''
    A set of known games
    '''

    _games = None
    _players = []


    def __init__(self, games):
        '''
        Construct the gameset. Require an iterable object of KnownGame
        '''
        
        if not hasattr(games, '__iter__'):
            raise TypeError("Parameter should be iterable")
        
        self._games = games
        
        playersSet = set()
        for game in self._games:
            if not isinstance(game, KnownGame):
                raise TypeError("Expected KnownGame (got %s)"%type(game))
            else:
                playersInGame = game.getPlayers()
                for player in playersInGame:
                    playersSet.add(player)
        self._playersList = list(playersSet)


    def averageNumberOfParticipations(self):
        
        playedGames = []
        for P in self._playersList:
            played = np.sum([1 for game in self._games if game.playerTeam(P)>0])
            playedGames.append(played)
        
        return np.mean(playedGames)
        
    def size(self):
        '''
        Return the number of games in the set
        '''
        return len(self._games)
    
    
    def getPlayers(self):
        '''
        Return the list of the involved players
        '''
        return self._playersList
    
    def getGameList(self):
        '''
        Return the list of the games in the dataset
        '''
        return self._games
    
    def __str__(self):
        return "GameSet with %s games and %s players"%(self.size(),len(self.getPlayers()))
    