'''
Created on 25 avr. 2014

@author: I066835
'''

from SkillGraph.model import Player

class Team(object):
    '''
    classdocs
    '''


    def __init__(self, iniContent = None):
        '''
        Constructor
        '''
        
        self._content = set()
        
        if iniContent!=None:
            self.add(iniContent)
        
    
    def add(self,obj):
        
        if isinstance(obj,Player):
            self._safeAdd(obj)
        elif hasattr(obj, '__iter__'):
            for value in obj:
                if isinstance(value,Player):
                    self._safeAdd(value)
                else:
                    raise TypeError("Expected Player but got %s"%type(obj))
                    
        else:
            raise TypeError("Expected Player but got %s"%type(obj))
        
    
    def size(self):
        return len(self._content)
    
    
    def getPlayers(self):
        '''
        return a list of the contained players
        '''
        return list(self._content)
    
    def containsPlayer(self,P):
        '''
        Returns true iff the player P is in the team
        '''
        return P in self._content
    
    # Private functions
            
    def _safeAdd(self,value):
        self._content.add(value)
        
    def __str__(self):
        return str(self._content)
    
    def __repr__(self):
        return str(self._content)
    
    @staticmethod
    def doIntersect(T1,T2):
        '''
        Return true if the teams have at least one common player
        '''
        for P in T1._content:
            if P in T2._content:
                return True
        return False
        
        
    